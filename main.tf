terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  region = "eu-west-3"
}

module "internet_gateway" {
  source = "./resources/igw"
  vpc_id = module.vpc.vpc_id
  igw_name = "TF-Internet-Gateway"
}

/* 
module "ec2_instances" {
  source = "./resources/ec2"
}

module "route_tables" {
  source = "./resources/rt"
}
*/

module "security_groups" {
  source      = "./resources/security_groups"
  name        = "my-security-group"
  vpc_id      = module.vpc.vpc_id

  ingress_rules = [
    {
      from_port   = 22 
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 5000
      to_port     = 5000
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]

  egress_rules = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]

  tags = {
    Name = "EC2 tf created secgrp"
  }
}

module "subnets" {
  source = "./resources/subnets"
  vpc_id     = module.vpc.vpc_id
  cidr_block = "10.0.1.0/24"
  subnet_name = "frontend-subnet"
}

module "vpc" {
  source = "./resources/vpc"
  cidr_block = "10.0.0.0/16"
  vpc_name = "TF-VPC"
}
