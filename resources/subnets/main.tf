resource "aws_subnet" "tf-public-subnet" {
  vpc_id            = var.vpc_id
  cidr_block        = var.cidr_block
  tags = {
    Name = var.subnet_name
  }
}

variable "vpc_id" {
  description = "VPC ID where the subnet will be created"
  type        = string
}

variable "cidr_block" {
  description = "CIDR block for the subnet"
  type        = string
}

variable "subnet_name" {
  description = "Name tag for the Subnet"
  type        = string
}