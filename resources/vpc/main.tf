resource "aws_vpc" "main" {
  cidr_block = var.cidr_block
  tags = {
    Name      = var.vpc_name
    terraform = true
  }
}

output "vpc_id" {
  value = aws_vpc.main.id
}

variable "cidr_block" {
  description = "CIDR block for the VPC"
  type        = string
}

variable "vpc_name" {
  description = "Name tag for the VPC"
  type        = string
}
