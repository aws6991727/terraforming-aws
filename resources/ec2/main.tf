resource "aws_instance" "tf-api-server" {
  ami = "ami-0cdfcb9783eb43c45"
  instance_type = "t2.large"
  subnet_id = aws_subnet.tf-public-subnet.id
  vpc_security_group_ids = [aws_security_group.tf-sg-api-server.id]
  associate_public_ip_address = true

  tags = {
    Name = "API Server"
  }
  user_data = file("./script.sh")
}
