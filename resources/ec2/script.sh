#!/bin/bash
cd /home/ec2-user
sudo yum -y upgrade
sudo yum install -y git
sudo yum install -y python3-pip
git clone https://github.com/0xdia/the-green-earth-post.git
pip3 install -r the-green-earth-post/back_end/requirements.txt

echo "AWS_DEFAULT_REGION=eu-west-3" | sudo tee -a /etc/environment
echo "MYSQL_DB_INSTANCE=greenearthpost-db" | sudo tee -a /etc/environment
echo "MYSQL_DATABASE=greenearthpost_database" | sudo tee -a /etc/environment
echo "MYSQL_USER=admin" | sudo tee -a /etc/environment
echo "MYSQL_PASSWORD=password" | sudo tee -a /etc/environment

source /etc/environment

gunicorn --bind 0.0.0.0:5000 --chdir the-green-earth-post/back_end handler:app
