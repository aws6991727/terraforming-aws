resource "aws_route_table" "tf-public_rt" {
  vpc_id = var.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.igw_id # aws_internet_gateway.tf-igw.id
  }
}

resource "aws_route_table_association" "public_rt_assoc" {
  subnet_id      = aws_subnet.tf-public-subnet.id
  route_table_id = aws_route_table.tf-public_rt.id
}

variable "vpc_id" {
  type = string
}

variable "igw_id" {
  type = string
}

