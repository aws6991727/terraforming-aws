resource "aws_internet_gateway" "tf-igw" {
  vpc_id = var.vpc_id
  tags = {
    Name = var.igw_name
  }
}

output "igw_id" {
  value = aws_internet_gateway.tf-igw.id
}
variable "igw_name" {
  description = "Internet gateway"
  type = string
}

variable "vpc_id" {
  description = "VPC ID"
  type = string
}