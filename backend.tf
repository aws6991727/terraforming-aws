terraform {
  backend "s3" {
    bucket = "terraforming-aws-tfstate"
    key    = "terraform.tfstate"
    region = "eu-west-3"
  }
}